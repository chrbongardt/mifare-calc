# Mifare access bits calculator

This project is demo of Qt Webassembly module. Mifare access bits calculator allows you to calculate access conditions by functional description and vice versa parse access conditions.
You can check the result at https://slebe.dev/mifarecalc/
