﻿import QtQuick 2.0
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.5

Item {
    id: componentRoot

    property var contentWidth: header.width

    implicitHeight: mainLayout.implicitHeight

    // block 0
    property bool c1_0
    property bool c2_0
    property bool c3_0
    // block 1
    property bool c1_1
    property bool c2_1
    property bool c3_1
    // block 2
    property bool c1_2
    property bool c2_2
    property bool c3_2

    ListModel {
        id: dataBlocksConfig
        ListElement { c1: false; c2: false; c3: false; read: "key A|B"; write: "key A|B"; increment: "key A|B"; decrement: "key A|B"; remarks: "default configuration" }
        ListElement { c1: false; c2: true;  c3: false; read: "key A|B"; write: "never";   increment: "never";   decrement: "never";   remarks: "read/write block" }
        ListElement { c1: true;  c2: false; c3: false; read: "key A|B"; write: "key B";   increment: "never";   decrement: "never";   remarks: "read/write block" }
        ListElement { c1: true;  c2: true;  c3: false; read: "key A|B"; write: "key B";   increment: "key B";   decrement: "key A|B"; remarks: "value block" }
        ListElement { c1: false; c2: false; c3: true;  read: "key A|B"; write: "never";   increment: "never";   decrement: "key A|B"; remarks: "value block" }
        ListElement { c1: false; c2: true;  c3: true;  read: "key B";   write: "key B";   increment: "never";   decrement: "never";   remarks: "read/write block" }
        ListElement { c1: true;  c2: false; c3: true;  read: "key B";   write: "never";   increment: "never";   decrement: "never";   remarks: "read/write block" }
        ListElement { c1: true;  c2: true;  c3: true;  read: "never";   write: "never";   increment: "never";   decrement: "never";   remarks: "read/write block" }
    }

    function decode() {
        for(var i = 0; i < dataBlocksConfig.count; ++i)
        {
            var config = dataBlocksConfig.get(i)
            if (config.c1 !== c1_0 || config.c2 !== c2_0 || config.c3 !== c3_0)
                continue;

            block0ConditionsRow.read = config.read
            block0ConditionsRow.write = config.write
            block0ConditionsRow.increment = config.increment
            block0ConditionsRow.decrement = config.decrement
            block0ConditionsRow.remarks = config.remarks
        }

        for(i = 0; i < dataBlocksConfig.count; ++i)
        {
            config = dataBlocksConfig.get(i)
            if (config.c1 !== c1_1 || config.c2 !== c2_1 || config.c3 !== c3_1)
                continue;

            block1ConditionsRow.read = config.read
            block1ConditionsRow.write = config.write
            block1ConditionsRow.increment = config.increment
            block1ConditionsRow.decrement = config.decrement
            block1ConditionsRow.remarks = config.remarks
        }

        for(i = 0; i < dataBlocksConfig.count; ++i)
        {
            config = dataBlocksConfig.get(i)
            if (config.c1 !== c1_2 || config.c2 !== c2_2 || config.c3 !== c3_2)
                continue;

            block2ConditionsRow.read = config.read
            block2ConditionsRow.write = config.write
            block2ConditionsRow.increment = config.increment
            block2ConditionsRow.decrement = config.decrement
            block2ConditionsRow.remarks = config.remarks
        }
    }

    ColumnLayout {
        id: mainLayout
        anchors.fill: parent

        RowLayout {
            id: header
            Layout.alignment: Qt.AlignHCenter
            spacing: 5

            Item {
                id: blockNumberHeader
                width: block0Header.implicitWidth + 20
            }

            CenteredText {
                id: readHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 100
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Read"
            }
            CenteredText {
                id: writeHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 100
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Write"
            }
            CenteredText {
                id: incrementHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 100
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Increment"
            }
            CenteredText {
                id: decrementHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 100
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Decrement,\ntransfer,\nrestore"
            }
            CenteredText {
                id: remarksHeader
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Layout.maximumWidth: 175
                Layout.minimumWidth: implicitWidth
                Layout.fillWidth: true
                text: "Remarks"
            }
            Item {
                id: editButtonHeader
                Layout.preferredWidth: 50
            }
        }

        //block 0
        Item {
            Layout.preferredWidth: header.width
            implicitHeight: block0ConditionsRow.implicitHeight
            Layout.alignment: Qt.AlignHCenter

            Row {
                id: block0ConditionsRow
                anchors.fill: parent
                spacing: header.spacing

                property string read: ""
                property string write: ""
                property string increment: ""
                property string decrement: ""
                property string remarks: ""

                CenteredText {
                    id: block0Header
                    width: blockNumberHeader.width
                    height: parent.height
                    text: "Block 0"
                }
                BlockConfigurationCell {
                    width: readHeader.width
                    text: parent.read
                }
                BlockConfigurationCell {
                    width: writeHeader.width
                    text: parent.write
                }
                BlockConfigurationCell {
                    width: incrementHeader.width
                    text: parent.increment
                }
                BlockConfigurationCell {
                    width: decrementHeader.width
                    text: parent.decrement
                }
                CenteredText {
                    width: remarksHeader.width
                    height: parent.height
                    text: parent.remarks
                }
                Button {
                    width: editButtonHeader.width
                    text: "Edit"
                    onClicked: {
                        editMenu.editingRowObject = block0ConditionsRow
                        editMenu.editingBlockNumber = 0
                        editMenu.popup(parent, 0, y + height)
                    }
                }
            }
        }

        //block 1
        Item {
            Layout.preferredWidth: header.width
            implicitHeight: block1ConditionsRow.implicitHeight
            Layout.alignment: Qt.AlignHCenter
            Row {
                id: block1ConditionsRow
                Layout.alignment: Qt.AlignHCenter
                spacing: header.spacing
                width: header.width

                property string read: ""
                property string write: ""
                property string increment: ""
                property string decrement: ""
                property string remarks: ""

                CenteredText {
                    width: blockNumberHeader.width
                    height: parent.height
                    text: "Block 1"
                }
                BlockConfigurationCell {
                    width: readHeader.width
                    text: parent.read
                }
                BlockConfigurationCell {
                    width: writeHeader.width
                    text: parent.write
                }
                BlockConfigurationCell {
                    width: incrementHeader.width
                    text: parent.increment
                }
                BlockConfigurationCell {
                    width: decrementHeader.width
                    text: parent.decrement
                }
                CenteredText {
                    width: remarksHeader.width
                    height: parent.height
                    text: parent.remarks
                }
                Button {
                    width: editButtonHeader.width
                    text: "Edit"
                    onClicked: {
                        editMenu.editingRowObject = block1ConditionsRow
                        editMenu.editingBlockNumber = 1
                        editMenu.popup(parent, 0, y + height)
                    }
                }
            }
        }

        //block 2
        Item {
            Layout.preferredWidth: header.width
            implicitHeight: block2ConditionsRow.implicitHeight
            Layout.alignment: Qt.AlignHCenter
            onImplicitWidthChanged: console.log("impl w " +implicitWidth)
            Row {
                id: block2ConditionsRow
                Layout.alignment: Qt.AlignHCenter
                spacing: header.spacing
                width: header.width

                property string read: ""
                property string write: ""
                property string increment: ""
                property string decrement: ""
                property string remarks: ""

                CenteredText {
                    width: blockNumberHeader.width
                    height: parent.height
                    text: "Block 2"
                }
                BlockConfigurationCell {
                    width: readHeader.width
                    text: parent.read
                }
                BlockConfigurationCell {
                    width: writeHeader.width
                    text: parent.write
                }
                BlockConfigurationCell {
                    width: incrementHeader.width
                    text: parent.increment
                }
                BlockConfigurationCell {
                    width: decrementHeader.width
                    text: parent.decrement
                }
                CenteredText {
                    width: remarksHeader.width
                    height: parent.height
                    text: parent.remarks
                }
                Button {
                    width: editButtonHeader.width
                    text: "Edit"
                    onClicked: {
                        editMenu.editingRowObject = block2ConditionsRow
                        editMenu.editingBlockNumber = 2
                        editMenu.popup(parent, 0, y + height)
                    }
                }
            }
        }
    }

    Menu {
        id: editMenu
        width: header.width

        property var editingRowObject: null
        property int editingBlockNumber: 0

        function applyConfig(config) {
            editingRowObject.read = config.read
            editingRowObject.write = config.write
            editingRowObject.increment = config.increment
            editingRowObject.decrement = config.decrement
            editingRowObject.remarks = config.remarks
            switch(editingBlockNumber)
            {
            case 0:
                componentRoot.c1_0 = config.c1
                componentRoot.c2_0 = config.c2
                componentRoot.c3_0 = config.c3
                break;
            case 1:
                componentRoot.c1_1 = config.c1
                componentRoot.c2_1 = config.c2
                componentRoot.c3_1 = config.c3
                break;
            case 2:
                componentRoot.c1_2 = config.c1
                componentRoot.c2_2 = config.c2
                componentRoot.c3_2 = config.c3
                break;
            }
        }

        Action {
            property var config: dataBlocksConfig.get(0)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: dataBlocksConfig.get(1)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: dataBlocksConfig.get(2)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: dataBlocksConfig.get(3)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: dataBlocksConfig.get(4)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: dataBlocksConfig.get(5)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: dataBlocksConfig.get(6)
            onTriggered: editMenu.applyConfig(config)
        }
        Action {
            property var config: dataBlocksConfig.get(7)
            onTriggered: editMenu.applyConfig(config)
        }

        delegate:
            MenuItem {
            id: menuItem
            background: Rectangle {
                implicitHeight: menuRow.implicitHeight + 20
                color:  menuItem.highlighted ? "grey" : "transparent"
                border.width: 0
                Row {
                    id: menuRow
                    anchors.topMargin: 10
                    anchors.bottomMargin: 10
                    anchors.fill: parent
                    spacing: 5
                    CenteredText {
                        width: readHeader.width
                        x: readHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.read
                    }
                    CenteredText {
                        width: writeHeader.width
                        x: writeHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.write
                    }
                    CenteredText {
                        width: incrementHeader.width
                        x: incrementHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.increment
                    }
                    CenteredText {
                        width: decrementHeader.width
                        x: decrementHeader.x
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.decrement
                    }
                    CenteredText {
                        anchors.verticalCenter: parent.verticalCenter
                        text: menuItem.action.config.remarks
                    }
                }
            }
        }
    }
}
